package com.mythics.webcenter.content.removexframeoptions;

import intradoc.common.ExecutionContext;
import intradoc.common.IdcStringBuilder;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.shared.FilterImplementor;
import intradoc.shared.SharedObjects;

/**
 * 
 * @author Jonathan Hult
 * 
 */
public class EditHttpResponseHeaderFilter implements FilterImplementor {

	private static final String COMPONENT_PREF_PROMPT = "RemoveXFrameOptions_Enabled";
	private static final String TRACE_SECTION = "removexframeoptions";

	private static final String FRAME_OPTIONS = "X-Frame-Options: SAMEORIGIN\r\n";
	private static final String XSS_PROTECTION = "X-XSS-Protection: 1; mode=block\r\n";
	private static final String CONTENT_TYPE_OPTIONS = "X-Content-Type-Options: nosniff\r\n";

	private static final String DASHES = "-----------------------";
	private static final String RESPONSE_BUFFER = "responseBuffer";

	@Override
	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext cxt) throws DataException, ServiceException {
		boolean isEnabled = SharedObjects.getEnvValueAsBoolean(COMPONENT_PREF_PROMPT, false);
		if (isEnabled) {
			if (cxt instanceof Service) {
				Service svc = (Service) cxt;
				String svcName = svc.getServiceData().m_name;
				trace(DASHES + "Service: " + svcName + DASHES);

				StringBuilder strBuilder = new StringBuilder(getRespBuffer(svc).toString());

				deleteStr(strBuilder, FRAME_OPTIONS);
				deleteStr(strBuilder, XSS_PROTECTION);
				deleteStr(strBuilder, CONTENT_TYPE_OPTIONS);

				IdcStringBuilder finalRespBuffer = new IdcStringBuilder(strBuilder.toString());
				svc.setCachedObject(RESPONSE_BUFFER, finalRespBuffer);
				trace("headers after removing X-Frame: " + getRespBuffer(svc).toString());
			}
		}
		return CONTINUE;
	}
	
	/**
	 * Get response buffer from Service.
	 * @param svc
	 * @return response buffer
	 */
	private IdcStringBuilder getRespBuffer(Service svc) {
		Object respBuffer = svc.getCachedObject(RESPONSE_BUFFER);
		IdcStringBuilder strBuilder;
		if (respBuffer instanceof IdcStringBuilder) {
			strBuilder = (IdcStringBuilder)respBuffer;
		} else {
			strBuilder = new IdcStringBuilder();
		}
		return strBuilder;
	}

	/**
	 * Delete a string from a StringBuilder
	 * @param builder
	 * @param str
	 */
	private void deleteStr(StringBuilder builder, String str) {
		int i = builder.indexOf(str);
		if (i != -1) {
			builder.delete(i, i + str.length());
		}
	}
	
	private void trace(String msg) {
		Report.trace(TRACE_SECTION, msg, null);
	}
}
