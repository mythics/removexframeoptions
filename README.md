﻿# RemoveXFrameOptions

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* Last Updated: build_1_20170623

## Overview
This component disables X-Frame options from being set which would cause frame-busting to happen.

This component implements an editHttpResponseHeader filter to remove any X-Frame options that may have been added for the HTTP response.

* Service Handlers:
	- Service: com.mythics.webcenter.content.removexframeoptions.EditHttpResponseHeaderFilter
	
* Filters:
	- editHttpResponseHeader: Executed after HTTP headers are loaded but before actual HTTP response

* Tracing Sections:
	- removexframeoptions
	
* Preference Prompts:
	- RemoveXFrameOptions_Enabled: Remove X-Frame options?
	
## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.9.0PSU-2017-05-03 08:49:28Z-r153417 (Build:7.3.5.185)

## Changelog
* build_1_20170623
	- Initial component release